
variable "dns_zone_name" {
    description = "The Cloud DNS zone created via prerequisites"
    default = "foo"
}
variable "fqdn" {
    description = "The URL for your app (must match what you use to create the SSL)"
    default = "vault.foo.com."
}
variable "project" { default = "" }
variable "region" { default = "" }
variable "zone" { default = "" }
variable "output_file" { default = "letsencrypt_dns_sa_creds.json.base64" }
variable "service_account_file" {
    description = "The key file to use for auth"
    default = "foo.json"
}

variable "service_account_json" {
    description = "json of service account pulled from var"
    default = ""
}

variable "vault_allowed_cidrs" {
    description = "List of CIDRs allowed to access the app"
    default = []
}