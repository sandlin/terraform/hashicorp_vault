# vault_tls_bucket=${vault_tls_bucket}
# vault_ca_cert_filename=${vault_ca_cert_filename}
# vault_tls_cert_filename=${vault_tls_cert_filename}
# vault_tls_key_filename=${vault_tls_key_filename}
# kms_project=${var.project}
# kms_crypto_key=${vault_tls_kms_key}

vault_tls_bucket=vault-certs
vault_ca_cert_filename=ca.crt
vault_tls_cert_filename=vault.crt
vault_tls_key_filename=vault.key.enc
kms_project=jsandlin-c9fe7132
kms_crypto_key=projects/jsandlin-c9fe7132/locations/us-west1/keyRings/vault/cryptoKeys/vault-init


# SRC: https://github.com/terraform-google-modules/terraform-google-vault/blob/master/modules/cluster/templates/startup.sh.tpl

gsutil cp "gs://${vault_tls_bucket}/${vault_ca_cert_filename}" /etc/vault.d/tls/ca.crt
gsutil cp "gs://${vault_tls_bucket}/${vault_tls_cert_filename}" /etc/vault.d/tls/vault.crt
gsutil cp "gs://${vault_tls_bucket}/${vault_tls_key_filename}" /etc/vault.d/tls/vault.key.enc

# Decrypt the Vault private key
base64 --decode < /etc/vault.d/tls/vault.key.enc | gcloud kms decrypt \
  --project="${kms_project}" \
  --key="${kms_crypto_key}" \
  --plaintext-file=/etc/vault.d/tls/vault.key \
  --ciphertext-file=-

# Make sure Vault owns everything
chmod 700 /etc/vault.d/tls
chmod 600 /etc/vault.d/tls/vault.key
chown -R vault:vault /etc/vault.d
rm /etc/vault.d/tls/vault.key.enc

# Add the TLS ca.crt to the trusted store so plugins dont error with TLS
# handshakes
cp /etc/vault.d/tls/ca.crt /usr/local/share/ca-certificates/
update-ca-certificates
